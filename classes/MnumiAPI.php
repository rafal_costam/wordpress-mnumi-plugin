<?php

require_once ('RestClient.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MnumiAPI {

    private $api_url;
    private $api_key;
    
    private $apiPaymeny = null;
    private $apiCarrier = null;
    
    private $apiProduct = array();
    
    private $apiProducts = null;
    private $apiCategory = null;
    private $apiProductList = null;
    
    private $apiProductFields = null;
    
    private $apiGroups = null;

    public function __construct($url, $key) {

        $this->api_key = $key;
        $this->api_url = $url . 'app.php/api/';
    }
    
    public function getKey()
    {
        return $this->api_key;
    }
    
    public function setOrder($params) {
        return RestClient::post($this->api_url . 'orders.json', json_encode($params), $this->api_key, $this->api_key, 'application/json')->getResponse();
    }

    public function getPayment($params) {
        if($this->apiPaymeny == null)
        {
            $this->apiPaymeny = RestClient::get($this->api_url . 'payments.json', $params, $this->api_key, $this->api_key)->getResponse();
        }
        return $this->apiPaymeny;
    }

    public function setPayment($params) {
        return RestClient::post($this->api_url . 'payments.json', json_encode($params), $this->api_key, $this->api_key)->getResponse();
    }

    public function getCarrier($params) {
        if($this->apiCarrier == null)
        {
            $this->apiCarrier = RestClient::get($this->api_url . 'carriers.json', $params, $this->api_key, $this->api_key)->getResponse();
        }
        return $this->apiCarrier;
    }

    public function setCarrier($params) {
        return RestClient::post($this->api_url . 'carriers.json', json_encode($params), $this->api_key, $this->api_key)->getResponse();
    }

    public function getClient($params) {
        return RestClient::get($this->api_url . 'clients.json', $params, $this->api_key, $this->api_key)->getResponse();
    }

    public function getUser($params) {
        return RestClient::get($this->api_url . 'users.json', $params, $this->api_key, $this->api_key)->getResponse();
    }

    public function setClient($params) {
        return RestClient::post($this->api_url . 'clients.json', json_encode($params), $this->api_key, $this->api_key, 'application/json')->getResponse();
    }
    
    public function setUser($params) {
        return RestClient::post($this->api_url . 'users.json', json_encode($params), $this->api_key, $this->api_key, 'application/json')->getResponse();
    }
    
    public function getProducts()
    {
        if($this->apiProducts == null)
        {
            $this->apiProducts = RestClient::get($this->api_url . 'products.json', null, $this->api_key, $this->api_key)
                    ->getResponse();
        }
        return $this->apiProducts;
    }
    
    public function getCategory()
    {
        if($this->apiCategory == null)
        {
            $this->apiCategory = RestClient::get($this->api_url . 'category.json', null, $this->api_key, $this->api_key)
                    ->getResponse();
        }
        return $this->apiCategory;
    }

    public function getProductsList() {
        if($this->apiProductList == null)
        {
            $categoryTmp = $this->getCategory();
            if (!$categoryTmp) {
                return false;
            }
            $category = array();
            foreach ($categoryTmp as $cat) {
                $category[$cat['id']] = $cat;
            }
            $products = $this->getProducts();
            if (!$products) {
                return false;
            }
            $return = array();
            foreach ($products as $product) {
                if (isset($category[(int) $product['category']]) && isset($product['slug'])) {
                    $return[(int) $product['category']]['name'] = $category[(int) $product['category']]['name'];
                    $return[(int) $product['category']]['products'][] = array(
                        'name' => (string) $product['name'],
                        'slug' => (string) $product['slug']
                    );
                }
            }
            $this->apiProductList = $return;
        }
        return $this->apiProductList;
    }

    public function getProduct($mnumi_id) {
        if($this->apiProduct == null)
        {
            $this->apiProduct = RestClient::get($this->api_url . 'products/' . $mnumi_id . '.json', null, $this->api_key, $this->api_key)->getResponse();
        }
        
        return $this->apiProduct;
    }

    private function getFieldType($name) {
        switch ($name) {
            case 'COUNT':
            case 'QUANTITY':
                return 'text';
                break;
            case 'SIZE':
            default:
                return 'select';
                break;
        }
    }
    
    public function getProductFields($mnumi_id)
    {
        if($this->apiProductFields == null)
        {
            $this->apiProductFields = RestClient::get($this->api_url . 'products/' . $mnumi_id . '/fields.json', null, $this->api_key, $this->api_key)
                    ->getResponse();
        }
        return $this->apiProductFields;
    }

    public function getGroups($mnumi_id) {
        if($this->apiGroups == null)
        {
            $fields = $this->getProductFields($mnumi_id);
            if (!$fields) {
                return false;
            }
            
            $groups = array();
            foreach ($fields as $field) {
                if (isset($field['default_value'])) {
                    if ((string) $field['fieldset']['label'] != "Wizard") {
                        $groups[(int) $field['id']] = array(
                            'group_type' => ((bool) $field['visible'] == false) ? 'hidden' : $this->getFieldType($field['fieldset']['name']),
                            'id' => $field['fieldset']['name'],
                            'default' => $field['default_value'],
                            'name' => ($field['label']) ? $field['label'] : $field['fieldset']['label']
                        );
                        if ($this->getFieldType($field['fieldset']['name']) == 'select') {
                            $params = array();
                            foreach ($field['field_items'] as $item) {
                                $params['key_' . $item['field_item']['id']] = array(
                                    'id' => $item['field_item']['id'],
                                    'name' => $item['field_item']['name'],
                                );
                            }
                            $groups[(int) $field['id']]['default'] = intval($field['default_value']);
                            foreach ($params as $attr) {
                                $groups[(int) $field['id']]['attributes'][(int) $attr['id']] = (string) $attr['name'];
                            }
                        }
                    }
                } elseif ((string) $field['fieldset']['label'] != "Wizard") {
                    $groups[(int) $field['id']] = array(
                        'group_type' => 'hidden',
                        'id' => $field['fieldset']['name'],
                        'name' => isset($field['label']) ? $field['label'] : $field['fieldset']['label']
                    );
                }
            }
            foreach ($groups as $key => $group)
            {
                if ($group['id'] == "GENERAL")
                {
                    unset($groups[$key]);
                }
            }
            $this->apiGroups = $groups;
        }
        return $this->apiGroups;
    }

    public function getCalculation($array, $product, $request = false) {
        foreach ($array as $name => $post) {
            if (is_numeric($name)) {
                $id = $name;
                $name = $post['name'];
                $post = $post['value'];
            }

            if (ctype_upper($name)) {
                if ($name == "OTHER") {
                    if (is_array($post)) {
                        foreach ($post as $key => $value) {
                            if (!isset($id) || $id == null) {
                                $postArray[$name][$key] = array('name' => $name, 'value' => $value);
                            } else {
                                $postArray[$name][$id] = array('name' => $name, 'value' => $value);
                            }
                        }
                    } else {
                        if ($id == null) {
                            $id = $name;
                        }
                        $postArray[$name][$id] = array('name' => $name, 'value' => $post);
                    }
                } else if ($name == "QUANTITY" || $name == "COUNT") {
                    $postArray[$name] = array('name' => $name, 'value' => (int) $post);
                } else {
                    if (is_array($post)) {
                        $postArray[$name] = $post;
                    } else {
                        $postArray[$name] = array('name' => $name, 'value' => $post);
                    }
                }
            } else if ($name == "qty") {
                $postArray['QUANTITY'] = array('name' => 'QUANTITY', 'value' => (int) $post);
            }
        }
        $postArray['NAME'] = array('name' => 'NAME', 'value' => 'targetAction');
        $postArray['ORDER_ID'] = array('name' => 'ORDER_ID', 'value' => 0);
        $postArray['INFORMATIONS'] = array('name' => 'INFORMATIONS', 'value' => null);
        $postArray['VALUE'] = array('name' => 'VALUE', 'value' => 'addNewWizard');
        if (isset($array['size_width']) && isset($array['size_height'])) {
            $postArray['SIZE']['width']['value'] = $array['size_width'];
            $postArray['SIZE']['height']['value'] = $array['size_height'];
            $postArray['SIZE']['metric']['value'] = "mm";
        }
        unset($postArray['FIXEDPRICE']);
        unset($postArray['GENERAL']);
        unset($postArray['PRODUCT']);
        try {
            $result = RestClient::post($this->api_url . 'calculations/' . $product . '.json', json_encode($postArray), $this->api_key, $this->api_key, 'application/json')
                    ->getResponse();
            if ($result) {
                return $result;
            }
        } catch (Exception $e) {
            
        }
    }

}
